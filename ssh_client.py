import paramiko

class SSH():
    def __init__(self):
        self._ssh_password = None
        self._ssh_username = None
        self._ssh_address = None
        self._ssh_port = None

        self._conn = None
        
    def connect_server(self, server_address, server_port, server_username, server_pass):
        self._ssh_password = server_pass
        self._ssh_username = server_username
        self._ssh_address = server_address
        self._ssh_port = server_port
        self._conn = paramiko.SSHClient()
        self._conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        self._conn.connect(hostname=self._ssh_address,
                    port=self._ssh_port,
                    username=self._ssh_username,
                    password=self._ssh_password)


    def execute_command(self, cmd, sudo=False):
        feed_password = False
        if sudo and self._ssh_username != 'root':
            cmd = "sudo -S -p '' %s" % cmd  # the prompt is written to the standard error, enter password in standard input
            feed_password = True
        stdin, stdout, stderr = self._conn.exec_command(command=cmd)
        if feed_password:
            stdin.write(self._ssh_password + "\n")
            stdin.flush()
        return {'out': stdout.readlines(), 
                'err': stderr.readlines(),
                'retval': stdout.channel.recv_exit_status()}
        

    def close(self):
        if self._conn is not None:
            self._conn.close()
            self._conn = None


    def get_username(self):
        return self._ssh_username